package max

import (
	"testing"
)



func TestFindMax(t *testing.T) {

	sliceInt := []interface{}{1, 5, 3}
	sliceFloat := []interface{}{5.3, 1.5, 3.2}
	type person struct {
		name string
		age  int
	}
	sliceStruct := []interface{}{
		person{"Denis", 36},
		person{"Alex", 28},
		person{"Liza", 32},
	}

	tests := []struct {
		name    string
		slice   []interface{}
		greater func(i, j int) bool
		want    interface{}
	}{
		{
			name:  "ints",
			slice: sliceInt,
			greater: func(i, j int) bool {
				return sliceInt[i].(int) > sliceInt[j].(int)
			},
			want: 5,
		},
		{
			name:  "floats",
			slice: sliceFloat,
			greater: func(i, j int) bool {
				return sliceFloat[i].(float64) > sliceFloat[j].(float64)
			},
			want: 5.3,
		},
		{
			name:  "structs age",
			slice: sliceStruct,
			greater: func(i, j int) bool {
				return sliceStruct[i].(person).age > sliceStruct[j].(person).age
			},
			want: person{"Denis", 36},
		},
		{
			name:  "structs name",
			slice: sliceStruct,
			greater: func(i, j int) bool {
				return sliceStruct[i].(person).name > sliceStruct[j].(person).name
			},
			want: person{"Liza", 32},
		},
		{
			name:  "empty slice",
			slice: []interface{}{},
			greater: func(i, j int) bool {
				return false
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := FindMax(tt.slice, tt.greater)
			if tt.want != got {
				t.Errorf("wanted %s not equal %s\n", tt.want, got)
			}
		})
	}
}
