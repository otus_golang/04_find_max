package max

func FindMax(slice []interface{}, compareFunc func(i, j int) bool) interface{} {
	if len(slice) == 0 {
		return nil
	}

	maxElement := 0

	for i := range slice {
		if compareFunc(i, maxElement) {
			maxElement = i
		}
	}

	return slice[maxElement]
}
